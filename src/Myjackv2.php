<?php

namespace Dhe\Myjackv2;

class Myjackv2
{
    public function greet(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}